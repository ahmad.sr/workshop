var express = require("express");
var router = express.Router();
const mongoose = require("mongoose");
var bcrypt = require("bcrypt");

var userModel = require("../models/user");

// ------------- getAll users --------------------
router.get("/", async function (req, res, next) {
  try {
    let users = await userModel.find();

    return res.send({
      message: "getAll users Success!",
      data: users,
      success: true,
    });
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false,
    });
  }
});

// ------------- getByID users -------------------
router.get("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "id invalid"
      });
    } else {
      let users = await userModel.findById(id);

      return res.send({
        message: "getByID users Success!",
        data: users,
        success: true
      });
    }
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false
    });
  }
});

// ------------- create users -------------------
router.post("/", async function (req, res, next) {
  try {
    let psw = req.body.password;
    let hash_psw = await bcrypt.hash(psw, 10);

    let new_user = new userModel({
      username: req.body.username,
      password: hash_psw,
      fname: req.body.fname,
      lname: req.body.lname,
      age: req.body.age,
      gender: req.body.gender,
    });

    let user = await new_user.save();

    return res.send({
      message: "Create New user Success!",
      // data: user,
      data: {
        _id: user._id,
        username: user.username,
        firstName: user.fname,
        lastName: user.lname,
        Age: user.age,
        Gender: user.gender,
      },
      success: true,
    });
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false,
    });
  }
});

// ------------- update users -------------------
router.put("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;
    let psw = req.body.password;
    let hash_psw = await bcrypt.hash(psw, 10);

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "id invalid",
      });
    } else {
      await userModel.updateOne(
        {
          _id: mongoose.Types.ObjectId(id),
        },
        {
          $set: {
            username: req.body.username,
            password: hash_psw,
            fname: req.body.fname,
            lname: req.body.lname,
            age: req.body.age,
            gender: req.body.gender,
          },
        }
      );

      let user = await userModel.findById(id);
      return res.send({
        message: "Update User Success!",
        data: user,
        success: true,
      });
    }
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false,
    });
  }
});

// ------------- delete users -------------------
router.delete("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "id invalid",
      });
    } else {
      await userModel.deleteOne({
        _id: mongoose.Types.ObjectId(id),
      });
      let users = await userModel.find();
      return res.status(200).send({
        message: "Delete User Success!",
        data: users,
        success: true,
      });
    }
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false,
    });
  }
});

module.exports = router;
