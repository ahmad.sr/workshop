var express = require("express");
var router = express.Router();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const multer = require("multer");

const userModel = require("../models/user");

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

// **** ---- การเข้าสู่ระบบ (Login ----- ****
router.post("/login", async function (req, res, next) {
  try {
    let user = await userModel.findOne({
      username: req.body.username,
    });

    if (!user) {
      return res.status(500).send({
        message: "login fail username",
        success: false,
      });
    }

    const checkPsw = await bcrypt.compare(req.body.password, user.password)
    if(!checkPsw){
      return res.status(500).send({
        message: "login fail password",
        success: false,
      });      
    }

    return res.status(200).send({
      message: "Login Success",
      data: {
        _id: user._id,
        username: user.username,
        firstName: user.fname,
        lastName: user.lname,
        age: user.age,
        gender: user.gender,
      },
      success: true,

    })

  } catch (err) {
    return res.status(500).send({
      message: "login fail",
      success: false,
    });
  }
});

// // --- การอัปโหลก ภาพ,ไฟล์ ---
// const storage = multer.diskStorage({
//   destination: function(req, file, cb){
//     cb(null,'./public/images/')
//   },
//   filename: function(req, file, cb){
//     cb(null,` ${file.originalname}-${new Date().getTime()}.jpg`)
//   }
// })
// const upload = multer({ storage : storage })

// router.post('/upload1',upload.single("img"), function(req, res, next) {
//   res.send({
//     message: "Upload .single Success!"
//   })

// })
// router.post('/upload2',upload.array("img",5), function(req, res, next) {
//   res.send({
//     message: "Upload .array Success!"
//   })

// })
// router.post('/upload3',upload.fields([{ name:"avatar", maxCount: 1 }, { name:"img", maxCount: 8 }])
// , function(req, res, next) {
//   res.send({
//     message: "Upload .fields Success! "
//   })

// })

// router.get('/token', async function(req, res, next) {
//   let payload ={
//     name : 'ahamd',
//     role : 'admin'
//   }
//   let token = jwt.sign(payload, 'key')
//   res.send({
//     token: token,
//     payload: payload
//   })

// })

// function decoded(req, res, next){
//   let token = req.headers.authorization.split('Bearer ')[1]
//   let decoded = jwt.verify(token, 'key')
//   if(decoded.role === 'admin'){
//     next()
//   }else{
//     res.status(401).send({
//       message: "no pass"
//     })
//   }
// }

// router.get('/decoded', decoded, async function(req, res, next) {
//   res.send({
//     message : "Hello admin"
//   })
// })

module.exports = router;
