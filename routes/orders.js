var express = require("express");
var router = express.Router();
const mongoose = require("mongoose");

var orderModel = require("../models/order");
var productModel = require("../models/product");

// ------------- getAll orders --------------------
router.get("/", async function (req, res, next) {
  try {
    let orders = await orderModel.find();

    return res.send({
      message: "getAll orders Success!",
      data: orders,
      success: true,
    });
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false,
    });
  }
});

// ------------- getByID orders -------------------
router.get("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "id invalid",
      });
    } else {
      let orders = await orderModel.findById(id);

      return res.send({
        message: "getByID orders Success!",
        data: orders,
        success: true,
      });
    }
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false,
    });
  }
});

// ------------- create orders -------------------
router.post("/", async function (req, res, next) {
  try {
    let orderList = req.body.orderList;
    let products = [];
    let newObjProducts = [];
    let tolalPrice = 0;
    let latestAmount = [];

    for (let i = 0; i < orderList.length; i++) {
      products[i] = await productModel.findById(orderList[i].product_id);

      newObjProducts[i] = {
        product_id: orderList[i].product_id,
        product_name: products[i].product_name,
        price: products[i].price,
        amount: orderList[i].amount,
        sum: products[i].price * orderList[i].amount,
      };
      // ---- ราคารวมทั้งหมด ----
      tolalPrice += products[i].price * orderList[i].amount;

      // --- เช็คจำนวนสินค้าว่าสั่งซื้อได้ไหม? ---
      latestAmount[i] = products[i].amount - orderList[i].amount;
      if (latestAmount[i] < 0) {
        console.log(`สินค้า ${products[i].product_name} มีจำนวนไม่เพียงพอ!`);
        return res.status(500).send({
          message: `สินค้า ${products[i].product_name} มีจำนวนไม่เพียงพอ!`,
          success: false,
        });
      }
    }

    for (let i = 0; i < orderList.length; i++) {
      products[i] = await productModel.findById(orderList[i].product_id);

      // --- อัพเดทจำนวนสินค้าที่ซื้อ ---
      latestAmount[i] = products[i].amount - orderList[i].amount;
      await productModel.updateOne(
        {
          _id: mongoose.Types.ObjectId(orderList[i].product_id),
        },
        {
          $set: {
            amount: latestAmount[i],
          },
        }
      );
    }

    let new_order = new orderModel({
      buyerName: req.body.buyerName,
      orderList: newObjProducts,
      tolalPrice: tolalPrice,
    });

    let order = await new_order.save();

    return res.send({
      message: "Create New order Success!",
      data: order,
      latest: "จำนวนสินค้าล่าสุด " + latestAmount,
      success: true,
    });
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false,
    });
  }
});

module.exports = router;
