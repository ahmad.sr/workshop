var express = require("express");
var router = express.Router();
const mongoose = require("mongoose");

var productModel = require("../models/product");

// ------------- getAll products --------------------
router.get("/", async function (req, res, next) {
  try {
    let products = await productModel.find();

    return res.send({
      message: "getAll products Success!",
      data: products,
      success: true
    });
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false
    });
  }
});

// ------------- getByID products -------------------
router.get("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "id invalid"
      });
    } else {
      let products = await productModel.findById(id);

      return res.send({
        message: "getByID products Success!",
        data: products,
        success: true
      });
    }
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false
    });
  }
});

// ------------- create products -------------------
router.post("/", async function (req, res, next) {
  try {
    let new_product = new productModel({
        product_name : req.body.product_name,
        price : req.body.price,
        amount : req.body.amount,
        detail : req.body.detail
    });

    let product = await new_product.save();

    return res.send({
      message: "Create New product Success!",
      data: product,
      success: true
    });
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false
    });
  }
});

// ------------- update products -------------------
router.put("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "id invalid"
      });
    } else {
      await productModel.updateOne(
        {
          _id: mongoose.Types.ObjectId(id),
        },
        {
          $set: {
            product_name : req.body.product_name,
            price : req.body.price,
            amount : req.body.amount,
            detail : req.body.detail
          }
        }
      );

      let product = await productModel.findById(id);
      return res.send({
        message: "Update Product Success!",
        data: product,
        success: true
      });
    }
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false
    });
  }
});

// ------------- delete products -------------------
router.delete("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "id invalid",
      });
    } else {
      await productModel.deleteOne({
        _id: mongoose.Types.ObjectId(id),
      })
      let products = await productModel.find();
      return res.status(200).send({
        message: "Delete Product Success!",
        data: products,
        success: true
      });
    }
  } catch (err) {
    console.log("ERROR!!!");
    return res.status(500).send({
      message: "error",
      success: false
    });
  }
});

module.exports = router;
