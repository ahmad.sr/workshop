const mongoose = require('mongoose');

const product = new mongoose.Schema({
    product_name : String,
    price : Number,
    amount : Number,
    detail : Object
})

module.exports = mongoose.model("products", product)